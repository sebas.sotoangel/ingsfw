class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None
        self.prev = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None

    def insert_at_end(self, data):
        if not self.head:
            new_node = Node(data)
            self.head = new_node
        else:
            current = self.head
            while current.next:
                current = current.next
            new_node = Node(data)
            current.next = new_node
            new_node.prev = current

    def delete_at_end(self):
        if not self.head:
            return
        if not self.head.next:
            self.head = None
        else:
            current = self.head
            while current.next:
                current = current.next
            current.prev.next = None
            current.prev = None

    def display(self):
        if not self.head:
            print("The list is empty")
            return
        current = self.head
        elements = []
        while current:
            elements.append(current.data)
            current = current.next
        print("Doubly Linked List: [" + " <-> ".join(map(str, elements)) + "]")

print("Esto funciona")